---
slug: "game-development"
date: "2020-11-09"
title: "Game Development"
publish: true
---

# Platforming

## Coyote jumping

When the character stops having a ground below it, give a it couple of frames of tolerance to allow it to jump, depending on the implementation or desired behaviour you may need to give an extra boost to compensate the gravity force applied on those frames.

Although I like the name convention for it I find it feels better to allow the character to fall while even though it can still jump as it feels more natural, instead of letting it keep walking as it there was an invisible floor.

```lua
floor=false
coyote=0

function update()
 past_floor=floor
 floor=check_floor()

 if(past_floor && !floor) then
  coyote=10
 end

 if(btn(jump_button)) and floor or coyote > 0 then
  do_jump()
 end

 coyote = max(0,coyote-1)
end

```

## Halved gravity jump


## Corner correction

## Swept collision detection

## Moving platforms

## Resources Dump

- [Full Celeste Twitter thread](https://twitter.com/mattthorson/status/1238338574220546049)
- [AABB + Swept collisions on GBC](https://eev.ee/blog/2018/11/28/cheezball-rising-collision-detection-part-1/)
- [Kyle Pulver nudge Assistance](http://kpulv.com/351/Platforming_Nudging_Assistance/)
- [Kyle Pulver coyote jumping](http://kpulv.com/123/Platforming_Ledge_Forgiveness/)
- [Rodrigo Monteiro The Guide to implementing 2D platformers](http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/)
- [Sonic collisions](https://info.sonicretro.org/SPG:Solid_Tiles)
- [Yoann Pignole Platformer controls](https://www.gamasutra.com/blogs/YoannPignole/20140103/207987/Platformer_controls_how_to_avoid_limpness_and_rigidity_feelings.php)
- [Advance Platform movemnt](https://www.instructables.com/id/Advanced-Platformer-Movement/)
- [Jumping Graphs For Platformer Games](http://www.davetech.co.uk/gamedevplatformer)
- [N++ Collision Detection and Response](https://www.metanetsoftware.com/2016/n-tutorial-a-collision-detection-and-response)
- [Why Does Celeste Feel So Good to Play? | Game Maker's Toolkit](https://www.youtube.com/watch?v=yorTG9at90g)
- [Celeste and TowerFall Physics](https://maddythorson.medium.com/celeste-and-towerfall-physics-d24bd2ae0fc5)

### PICO-8

- [Nerdy Teachers Platformer setup](https://www.youtube.com/watch?v=q6c6DvGK4lg&amp;amp;list=PLyhkEEoUjSQtUiSOu-N4BIrHBFtLNjkyE)
- [Advanced Micro Platformer - Starter Kit](https://www.lexaloffle.com/bbs/?tid=28793)
