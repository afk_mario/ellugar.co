---
slug: "software"
date: "2021-02-17"
title: "Software"
publish: true
---

# Sway

## WEBRTC screen share

For a long time screen sharing wasn't working, I have a simple script that exports the wayland related env variables before starting sway but it seems that you need to manually call `systemctl --user import-enviroment XDG_CURRENT_DESKTOP` for them to be available for process started by systemd

### Resources 

- [Test page](https://mozilla.github.io/webrtc-landing/gum_test.html)
- [System user services and env variables](https://github.com/emersion/xdg-desktop-portal-wlr/wiki/systemd-user-services,-pam,-and-environment-variables)
- [Make screen sharing on wayland (sway) work!](https://soyuka.me/make-screen-sharing-wayland-sway-work/)
- [Troubleshooting guide](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Troubleshooting)
- [Arch Linux Wiki](https://wiki.archlinux.org/index.php/PipeWire)
- [Wayland in 2021](https://shibumi.dev/posts/wayland-in-2021/)

# Web browser

## Firefox

`about:config` flags I change on Firefox

- `media.hardwaremediakeys.enabled`: Disable media controls, I prefer controling local media
- `browser.link.open_newwindow.restriction`: Force all new links open in tab [link](https://support.mozilla.org/en-US/questions/1258193)
